#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <linux/input.h>
#include <libgen.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>

// right windows key
#define TRIGGER_KEY 126

struct shortcut {
	unsigned short key;
	unsigned char mod;
	struct {
		bool quick_bind;
	} flags;
	const char *path;
	const char **argv;
};

// potentially could be changed to a ((1 << 16) - 1) long array
// since the "code" value in input_event struct is an unsigned short
// but I don't feel like wasting half a megabyte of RAM on empty pointers
unsigned short shortcut_count;
struct shortcut* shortcuts;
unsigned char enabled = 0;

const char* device = "/dev/input/event";

char* conf_file;
char* input_device;

void load_config(char* path) {
	FILE *fp = fopen(path, "r");
	if (!fp)
		return;

	unsigned short cnt;
	char c;
	while (fread(&c, 1, 1, fp) > 0) {
		if (c == '}')
			cnt++;
	}
	shortcut_count = cnt;
	shortcuts = calloc(cnt, sizeof(struct shortcut));

	char lc;
	char buf[256];
	struct string {
		struct string* next;
		char* string;
	};
	struct string* strings = NULL;
	struct string* last_string = strings;
	unsigned char ste = 0;
	unsigned char bi = 0, sc = 0;
	cnt = 0;
	unsigned short key;
	rewind(fp);
	while (fread(&c, 1, 1, fp) > 0) {
		if (lc != '\\') {
			if (c == '{') {
				shortcuts[cnt].key = atoi(buf);
				bi = 0;
				ste = 1;
			} else if (c == '}') {
				buf[bi] = 0;
				struct shortcut *c = &shortcuts[cnt++];
				struct string* s = strings;
				c->argv = calloc(1, sc + 1);
				unsigned char cai = 0;
				while (s) {
					if (!c->path) {
						c->path = s->string;
						c->argv[cai++] = basename(s->string);
					} else {
						c->argv[cai++] = s->string;
					}
					struct string* b = s->next;
					free(s);
					s = b;
				}
				last_string = NULL;
				strings = NULL;
				bi = 0;
				sc = 0;
				ste = 0;
			} else if (c == '"') {
				if (ste == 1) {
					ste = 2;
					bi = 0;
				} else if (ste == 2) {
					ste = 1;
					sc++;
					struct string* s = calloc(1, sizeof(struct string));
					s->string = calloc(1, bi + 1);
					memcpy(s->string, buf, bi);
					if (strings == NULL) {
						strings = s;
						last_string = strings;
					} else {
						last_string->next = s;
						last_string = s;
					}
				}
				continue;
			}
		}
		if (ste == 2)
			buf[bi++] = c;
		if (ste == 0) {
			if (isdigit(c))
				buf[bi++] = c;
			else if (c == '\'')
				shortcuts[cnt].flags.quick_bind = true;
		}
		lc = c;
	}
}

int main(int argc, char** argv) {
	if (argc != 3) {
		printf("This program requires TWO (2) arguments: <configuration file> <input event device>.\n");
		return 0;
	}

	if (access(argv[1], F_OK) < 0) {
		printf("Couldn't access configuration file %s: %s.\n", argv[1], strerror(errno));
		return -1;
	}

	if (!atoi(argv[2])) {
		printf("The second argument be a number. Got: %s\n", argv[2]);
		return -1;
	}

	// the number should almost never be longer than 2 characters, but still.
	// (though I do want to know if someone does have over 100 event devices)
	char buff[8];
	sprintf(&buff[0], "%d", atoi(argv[2]));
	input_device = malloc(strlen(device) + strlen(buff) + 1);
	strcpy(input_device, device);
	strcat(input_device, buff);
	signal(SIGCHLD,SIG_IGN);
	conf_file = argv[1];
	load_config(conf_file);

	int ifd = open(input_device, O_RDWR);
	if (!ifd)
		return -1;

	struct input_event esyn;
	esyn.type = EV_SYN;
	esyn.code = esyn.value = 0;
	struct input_event ev;
	while (1) {
    		read(ifd, &ev, sizeof(ev));
    		if (ev.type != EV_KEY)
			continue;
		int cod = ev.code;
		if (ev.value != 1)
			continue;
		if (cod == TRIGGER_KEY) {
			ev.value = 0;
			write(ifd, &ev, sizeof(ev)); // Release?
			write(ifd, &esyn, sizeof(esyn));
			enabled ^= 1;
			ioctl(ifd, EVIOCGRAB, enabled);
		} else if (enabled) {
			for (unsigned short i = 0; i < shortcut_count; i++) {
				struct shortcut sc = shortcuts[i];
				if (sc.key != cod)
					continue;
				if (!vfork()) {
					execv(sc.path, (char *const *)sc.argv);
					exit(0);
				}
				if (sc.flags.quick_bind) {
					puts("Quick Bind");
					enabled = 0;
					ioctl(ifd, EVIOCGRAB, enabled);
					break;
				}
			}
		}
  	}
}
